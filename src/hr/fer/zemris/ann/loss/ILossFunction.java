package hr.fer.zemris.ann.loss;

import hr.fer.zemris.ann.structures.My2DMatrix;

public interface ILossFunction {
    double loss(My2DMatrix realY, My2DMatrix predictedY);
}
