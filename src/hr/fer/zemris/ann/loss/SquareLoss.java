package hr.fer.zemris.ann.loss;


import hr.fer.zemris.ann.structures.My2DMatrix;

import java.io.Serializable;

public class SquareLoss implements ILossFunction, Serializable {
    private static final long serialVersionUID = 5L;
    @Override
    public double loss(My2DMatrix realY, My2DMatrix predictedY) {
        assert realY.numberOfCols() == 1 && predictedY.numberOfCols() == 1;
        double squareSum = 0;
        double[] realYArr = realY.getColumn(0);
        double[] predictedYArr = predictedY.getColumn(0);

        for (int i = 0; i < realYArr.length; i++) {
            squareSum += Math.pow(realYArr[i] - predictedYArr[i], 2);
        }
        return squareSum;
    }
}
