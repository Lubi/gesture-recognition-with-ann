package hr.fer.zemris.ann.structures;

import java.util.Objects;

public class Example {

    My2DMatrix x;
    My2DMatrix y;

    public Example(My2DMatrix x, My2DMatrix y) {
        assert x.numberOfCols() == 1 && y.numberOfCols() == 1;
        this.x = x;
        this.y = y;
    }

    public My2DMatrix getX() {
        return x;
    }

    public My2DMatrix getY() {
        return y;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Example that = (Example) o;
        return x.equals(that.getX()) && y.equals( that.getY());
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }
}
