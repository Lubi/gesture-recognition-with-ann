package hr.fer.zemris.ann.structures;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class My2DMatrix  implements Serializable {
    private static final long serialVersionUID = 3L;

    private double[][] values;

    public My2DMatrix(double[][] values) {
        this.values = values;
    }

    // vaktor sa svim vrijednostima value
    public My2DMatrix(int N, double value) {
        values = new double[N][1];
        for (int i = 0; i < N; i++) {
            values[i][0] = value;
        }
    }

    //init on zeros
    public My2DMatrix(int N, int M) {
        values = new double[N][M];
    }

    public My2DMatrix(int N, int M, double[] range) {
        assert range.length == 2 && range[0] <= range[1];

        double min = range[0];
        double max = range[1];

        values = new double[N][M];
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < M; j++) {
                double randValue = min + (Math.random() * (max - min));
                values[i][j] = randValue;
            }
        }

    }

    public int numberOfCols() {
        return values[0].length;
    }

    public int numberOfRows() {
        return values.length;
    }

    public double[] getRow(int index) {
        assert index >= 0 && index < numberOfRows();
        return values[index];
    }

    public double[] getColumn(int index) {
        assert index >= 0 && index < numberOfCols();

        double[] column = new double[numberOfRows()];

        for (int i = 0; i < numberOfRows(); i++) {
            column[i] = values[i][index];
        }
        return column;
    }

    public double getValueAt(int i, int j) {
        return values[i][j];
    }

    public double[][] getValues() {
        return values;
    }

    public My2DMatrix mulWith(My2DMatrix second) {

        double[][] firstMatrix = this.values;
        double[][] secondMatrix = second.values;
        double[][] result = new double[firstMatrix.length][secondMatrix[0].length];

        for (int row = 0; row < result.length; row++) {
            for (int col = 0; col < result[row].length; col++) {
                result[row][col] = multiplyMatricesCell(firstMatrix, secondMatrix, row, col);
            }
        }

        return new My2DMatrix(result);
    }

    private double multiplyMatricesCell(double[][] firstMatrix, double[][] secondMatrix, int row, int col) {
        double cell = 0;
        for (int i = 0; i < secondMatrix.length; i++) {
            cell += firstMatrix[row][i] * secondMatrix[i][col];
        }
        return cell;
    }

    public My2DMatrix mulElementWise(My2DMatrix second) {
        assert Arrays.equals(this.getDimensions().toArray(), second.getDimensions().toArray());

        double[][] newArr = new double[this.numberOfRows()][this.numberOfCols()];

        for (int i = 0; i < numberOfRows(); i++) {
            for (int j = 0; j < numberOfCols(); j++) {
                newArr[i][j] = this.values[i][j] * second.getValueAt(i, j);
            }
        }
        return new My2DMatrix(newArr);
    }

    public My2DMatrix mulWithScalar(double scalar) {
        double[][] newArr = new double[this.numberOfRows()][this.numberOfCols()];

        for (int i = 0; i < numberOfRows(); i++) {
            for (int j = 0; j < numberOfCols(); j++) {
                newArr[i][j] = this.values[i][j] * scalar;
            }
        }
        return new My2DMatrix(newArr);
    }

    public My2DMatrix add(My2DMatrix second) {
        assert Arrays.equals(this.getDimensions().toArray(), second.getDimensions().toArray());

        double[][] newArr = new double[this.numberOfRows()][this.numberOfCols()];

        for (int i = 0; i < numberOfRows(); i++) {
            for (int j = 0; j < numberOfCols(); j++) {
                newArr[i][j] = this.values[i][j] + second.getValueAt(i, j);
            }
        }
        return new My2DMatrix(newArr);
    }

    public My2DMatrix subtract(My2DMatrix second) {
        assert Arrays.equals(this.getDimensions().toArray(), second.getDimensions().toArray());

        double[][] newArr = new double[this.numberOfRows()][this.numberOfCols()];

        for (int i = 0; i < numberOfRows(); i++) {
            for (int j = 0; j < numberOfCols(); j++) {
                newArr[i][j] = this.values[i][j] - second.getValueAt(i, j);
            }
        }
        return new My2DMatrix(newArr);
    }

    public My2DMatrix transpose() {
        double[][] newArr = new double[numberOfCols()][numberOfRows()];

        for (int i = 0; i < numberOfRows(); i++) {
            for (int j = 0; j < numberOfCols(); j++) {
                newArr[j][i] = values[i][j];
            }
        }
        return new My2DMatrix(newArr);
    }

    public My2DMatrix copy() {

        final double[][] result = new double[this.numberOfRows()][this.numberOfCols()];
        for (int i = 0; i < values.length; i++) {
            result[i] = Arrays.copyOf(values[i], values[i].length);
        }


        return new My2DMatrix(result);
    }

    public List<Integer> getDimensions() {
        List<Integer> dimensions = new ArrayList<>();
        dimensions.add(this.numberOfRows());
        dimensions.add(this.numberOfCols());
        return dimensions;
    }

    public void set(int i, int j, double val) {
        assert i >= 0 && i < numberOfRows() && j >= 0 && j < numberOfCols();
        values[i][j] = val;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < numberOfRows(); i++) {
            String result = Arrays.stream(getRow(i))
                    .mapToObj(Double::toString)
                    .collect(Collectors.joining("\t"));
            stringBuilder.append(result).append("\n");
        }
        return stringBuilder.toString();
    }

    public List<Integer> getIndexOfMaxVal() {
        List<Integer> maxIndices = new ArrayList<Integer>(List.of(0,0));
        double max = values[0][0];
        for (int i = 0; i < values.length; i++) {
            for (int j = 0; j < values[0].length; j++) {
                if (values[i][j] > max) {
                    max = values[i][j];
                    maxIndices = new ArrayList<Integer>(List.of(i, j));
                }
            }
        }
        return maxIndices;
    }
}
