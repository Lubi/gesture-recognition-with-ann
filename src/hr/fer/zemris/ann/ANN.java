package hr.fer.zemris.ann;

import hr.fer.zemris.ann.activationfunctions.IActivationFunction;
import hr.fer.zemris.ann.loss.ILossFunction;
import hr.fer.zemris.ann.structures.Example;
import hr.fer.zemris.ann.structures.My2DMatrix;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ANN implements Serializable {
    private static final long serialVersionUID = 1L;
    private final double[] range = {-1, 1};
    private List<List<My2DMatrix>> errorMatrices;

    private List<Integer> annDimension;
    private List<My2DMatrix> biases;
    private ILossFunction lossFunction;
    private List<My2DMatrix> weights;
    private List<My2DMatrix> intermediateResult; // nakon forward propagacije
    private IActivationFunction activationFunction;


    public ANN(List<Integer> annDimension, ILossFunction lossFunction, IActivationFunction activationFunction) {
        this.lossFunction = lossFunction;
        this.annDimension = annDimension;
        this.activationFunction = activationFunction;
    }


    public void storeANNToFile(String filePath) {
        try {
            FileOutputStream f = new FileOutputStream(new File(filePath));
            ObjectOutputStream o = new ObjectOutputStream(f);

            // Write object to file
            o.writeObject(this);

            o.close();
            f.close();
        } catch (FileNotFoundException e) {
            System.out.println("File not found");
        } catch (IOException e) {
            System.out.println("Error initializing stream");
        }
    }

    public My2DMatrix predict(My2DMatrix x) {
        return forwardPropagation(x);
    }

    public void fit(Dataset dataset, int batchSize, int epochs, double learningRate, double epsilon) {
        initWeights();
        initErrors();
        initBiases();


        for (int i = 0; i < epochs; i++) {
            var randomAccess = generateRandomIndices(dataset.size());

            for (int j = 0; j < (dataset.size() / batchSize); j++) {
                for (int k = 0; k < batchSize; k++) {
                    Example current = dataset.getAtIndex(randomAccess.get(j * batchSize + k)); //randomAccess je proxy na dataset
                    My2DMatrix onePredict = forwardPropagation(current.getX());
                    assert onePredict.numberOfCols() == 1;
                    updateErrorsMatrices(onePredict, current.getY());
                }
                backPropagation(learningRate);
                initErrors(); //poništiti pogrešku nakon ažuriranja težina
            }

            double errorOnDataset = evaluateAtEndOfEpoch(dataset);
            printError(i, errorOnDataset);
            if (errorOnDataset < epsilon) {
                return;
            }
        }


    }

    private List<Integer> generateRandomIndices(int size) {
        var randomIndices = new ArrayList<Integer>();

        for (int i = 0; i < size; i++) {
            randomIndices.add(i);
        }
        Collections.shuffle(randomIndices);
        return randomIndices;
    }

    private My2DMatrix forwardPropagation(My2DMatrix x) {

        My2DMatrix outputOfPreviousLayer = x;
        intermediateResult = new ArrayList<>();
        intermediateResult.add(outputOfPreviousLayer.copy());
        for (int i = 1; i < numberOfLayers(); i++) {
            My2DMatrix W = getWOfLayer(i);
            My2DMatrix B = getBOfLayer(i);
            My2DMatrix net = (W.mulWith(outputOfPreviousLayer)).subtract(B);
            outputOfPreviousLayer = activationFunction.activate(net);
            intermediateResult.add(outputOfPreviousLayer.copy());
        }
        return outputOfPreviousLayer.copy();
    }

    private void updateErrorsMatrices(My2DMatrix predictY, My2DMatrix realY) {
        //posebno završni sloj
        int endLayer = numberOfLayers() - 1;

        My2DMatrix EASetEnd = predictY.subtract(realY);
        setErrorAOfLayer(endLayer, EASetEnd);

        My2DMatrix matrixOnes = new My2DMatrix(predictY.numberOfRows(), 1.0);
        My2DMatrix oneMinusPredicted = matrixOnes.subtract(predictY);
        My2DMatrix EISetEnd = (EASetEnd.mulElementWise(predictY)).mulElementWise(oneMinusPredicted);
        setErrorIOfLayer(endLayer, EISetEnd);

        setErrorWOfLayer(endLayer, EISetEnd.mulWith(getIntermediateResult(endLayer - 1).transpose()));

        setErrorBOfLayer(endLayer, EISetEnd.mulWithScalar(-1.0));

        for (int i = numberOfLayers() - 2; i > 0; i--) {
            My2DMatrix EASet = (getWOfLayer(i + 1).transpose()).mulWith(getErrorIOfLayer(i + 1));
            setErrorAOfLayer(i, EASet);

            My2DMatrix curentIntermRes = intermediateResult.get(i);
            My2DMatrix matrixOnesHidden = new My2DMatrix(curentIntermRes.numberOfRows(), 1.0);
            My2DMatrix EISet = (EASet.mulElementWise(curentIntermRes)).mulElementWise(matrixOnesHidden.subtract(curentIntermRes));
            setErrorIOfLayer(i, EISet);

            My2DMatrix previousIntermediateRes = intermediateResult.get(i - 1);
            My2DMatrix EWSet = EISet.mulWith(previousIntermediateRes.transpose());
            setErrorWOfLayer(i, EWSet);

            setErrorBOfLayer(i, EISet.mulWithScalar(-1));
        }
    }

    private void backPropagation(double learningRate) {

        for (int i = numberOfLayers() - 1; i > 0; i--) {
            My2DMatrix EW = getErrorWOfLayer(i);
            My2DMatrix newW = getWOfLayer(i).subtract(EW.mulWithScalar(learningRate));
            setWOfLayer(i, newW);

            //set new bias
            My2DMatrix EB = getErrorBOfLayer(i);
            My2DMatrix newB = getBOfLayer(i).subtract(EB.mulWithScalar(learningRate));
            setBOfLayer(i, newB);
        }
    }

    private double evaluateAtEndOfEpoch(Dataset dataset) {
        double errorSum = 0;
        for (int i = 0; i < dataset.size(); i++) {
            var example = dataset.getAtIndex(i);
            var predicted = forwardPropagation(example.getX());
            errorSum += lossFunction.loss(example.getY(), predicted);
        }
        return errorSum / dataset.size();
    }

    private void printError(int currentEpoch, double errorOnDataset) {
        System.out.println("Current_epoch= " + currentEpoch + " error= " + errorOnDataset);
    }

    private void initBiases() {
        biases = new ArrayList<>();

        for (int i = 1; i < numberOfLayers(); i++) {
            My2DMatrix B = new My2DMatrix(annDimension.get(i), 1);
            biases.add(B);
        }
    }

    private void initErrors() {
        errorMatrices = new ArrayList<>();

        for (int i = 1; i < numberOfLayers(); i++) {
            List<My2DMatrix> errorByLayer = new ArrayList<>();
            My2DMatrix EA = new My2DMatrix(annDimension.get(i), 1);
            My2DMatrix EI = new My2DMatrix(annDimension.get(i), 1);
            My2DMatrix EW = new My2DMatrix(annDimension.get(i), annDimension.get(i - 1));
            My2DMatrix EB = new My2DMatrix(annDimension.get(i), 1);
            errorByLayer.add(EA);
            errorByLayer.add(EI);
            errorByLayer.add(EW);
            errorByLayer.add(EB);
            errorMatrices.add(errorByLayer);
        }
    }


    private void initWeights() {
        weights = new ArrayList<>();

        for (int i = 1; i < numberOfLayers(); i++) {
            My2DMatrix W = new My2DMatrix(annDimension.get(i), annDimension.get(i - 1), range);
            weights.add(W);
        }
    }

    private My2DMatrix getErrorAOfLayer(int index) {
        assert index >= 1 && index < numberOfLayers();
        return errorMatrices.get(index - 1).get(0);
    }

    private My2DMatrix getErrorIOfLayer(int index) {
        assert index >= 1 && index < numberOfLayers();
        return errorMatrices.get(index - 1).get(1);
    }

    private My2DMatrix getErrorWOfLayer(int index) {
        assert index >= 1 && index < numberOfLayers();
        return errorMatrices.get(index - 1).get(2);
    }

    private My2DMatrix getErrorBOfLayer(int index) {
        assert index >= 1 && index < numberOfLayers();
        return errorMatrices.get(index - 1).get(3);
    }

    private My2DMatrix getWOfLayer(int index) {
        assert index >= 1 && index < numberOfLayers();
        return weights.get(index - 1);
    }

    private My2DMatrix getBOfLayer(int index) {
        assert index >= 1 && index < numberOfLayers();
        return biases.get(index - 1);
    }

    private void setErrorAOfLayer(int index, My2DMatrix newMatrix) {
        assert index >= 1 && index < numberOfLayers();
        errorMatrices.get(index - 1).set(0, newMatrix);
    }

    private void setErrorIOfLayer(int index, My2DMatrix newMatrix) {
        assert index >= 1 && index < numberOfLayers();
        errorMatrices.get(index - 1).set(1, newMatrix);
    }

    private void setErrorWOfLayer(int index, My2DMatrix newMatrix) {
        assert index >= 1 && index < numberOfLayers();
        errorMatrices.get(index - 1).set(2, newMatrix);
    }

    private void setErrorBOfLayer(int index, My2DMatrix newMatrix) {
        assert index >= 1 && index < numberOfLayers();
        errorMatrices.get(index - 1).set(3, newMatrix);
    }

    private void setWOfLayer(int index, My2DMatrix newMatrix) {
        assert index >= 1 && index < numberOfLayers();
        weights.set(index - 1, newMatrix);
    }

    private void setBOfLayer(int index, My2DMatrix newMatrix) {
        assert index >= 1 && index < numberOfLayers();
        biases.set(index - 1, newMatrix);
    }

    private My2DMatrix getIntermediateResult(int index) {
        assert index >= 0 && index < numberOfLayers();
        assert intermediateResult != null;
        return intermediateResult.get(index);
    }

    public int numberOfLayers() {
        return annDimension.size();
    }
}
