package hr.fer.zemris.ann;

import hr.fer.zemris.ann.eventhandler.IGUIEventHandler;
import hr.fer.zemris.ann.eventhandler.TesterGUIEventHandler;
import hr.fer.zemris.ann.gui.MyFrame;
import hr.fer.zemris.ann.transform.Transformer;

import javax.swing.*;
import java.io.*;
import java.util.Arrays;
import java.util.HashMap;
import java.util.stream.Collectors;

public class TesterMain {
    public static void main(String[] args) {

        final int discretePointsNum = Integer.parseInt(args[0]);
        var ann = readANNFromFile(args[1]);

        String[] gestures = {"alpha", "beta", "gamma", "delta", "epsilon"};

        HashMap<String, String> gestureCodes = addGestureCodes(gestures);
        Transformer transformer = new Transformer(discretePointsNum);
        IGUIEventHandler testerGUIEventHandler = new TesterGUIEventHandler(transformer, gestureCodes, ann);

        JFrame gesturesFrame = new MyFrame(gestures, testerGUIEventHandler);
        gesturesFrame.setVisible(true);
    }


    private static HashMap<String, String> addGestureCodes(String[] gestures) {
        HashMap<String, String> result = new HashMap<>();

        for (int i = 0; i < gestures.length; i++) {
            int[] newCode = new int[gestures.length];
            newCode[i] = 1;
            String makeString = Arrays.stream(newCode)
                    .mapToObj(String::valueOf)
                    .collect(Collectors.joining(","));
            result.put(String.join(",", makeString), gestures[i]);
        }

        return result;
    }

    private static ANN readANNFromFile(String annFile) {
        ANN ann = null;
        try {

            FileInputStream fi = new FileInputStream(new File(annFile));
            ObjectInputStream oi = new ObjectInputStream(fi);

            // Read object
            ann = (ANN) oi.readObject();

            oi.close();
            fi.close();

        } catch (FileNotFoundException e) {
            System.out.println("File not found");
        } catch (IOException e) {
            System.out.println("Error initializing stream");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }


        return ann;
    }
}
