package hr.fer.zemris.ann;


import hr.fer.zemris.ann.eventhandler.IGUIEventHandler;
import hr.fer.zemris.ann.eventhandler.InputGUIEventHandler;
import hr.fer.zemris.ann.gui.MyFrame;
import hr.fer.zemris.ann.transform.Transformer;

import javax.swing.*;
import java.util.Arrays;
import java.util.HashMap;
import java.util.stream.Collectors;

public class InputMain {
    public static void main(String[] args) {
        final int gesturesNumPerClass = Integer.parseInt(args[0]);
        final int discretePointsNum = Integer.parseInt(args[1]);
        String filePath = args[2];

        String[] gestures = {"alpha", "beta", "gamma", "delta", "epsilon"};

        HashMap<String, String> gestureCodes = addGestureCodes(gestures);
        Transformer transformer = new Transformer(discretePointsNum);
        IGUIEventHandler inputGUIHandler = new InputGUIEventHandler(transformer, filePath, gestureCodes, gesturesNumPerClass);

        JFrame gesturesFrame = new MyFrame(gestures, inputGUIHandler);
        gesturesFrame.setVisible(true);


    }

    private static HashMap<String, String> addGestureCodes(String[] gestures) {
        HashMap<String, String> result = new HashMap<>();

        for (int i = 0; i < gestures.length; i++) {
            int[] newCode = new int[gestures.length];
            newCode[i] = 1;
            String makeString = Arrays.stream(newCode)
                    .mapToObj(String::valueOf)
                    .collect(Collectors.joining(","));
            result.put(gestures[i], String.join(",", makeString));
        }

        return result;
    }
}
