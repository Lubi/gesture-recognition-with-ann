package hr.fer.zemris.ann;

import hr.fer.zemris.ann.structures.Example;
import hr.fer.zemris.ann.structures.My2DMatrix;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Dataset {
    List<Example> examples;


    public Dataset(List<Example> examples) {
        this.examples = examples;

    }

    public Dataset(String filePath) {
        this.examples = makeDataset(filePath);
    }

    private static List<Example> makeDataset(String filePath) {


        List<Example> result = new ArrayList<>();

        BufferedReader reader;
        try {
            reader = new BufferedReader(new FileReader(filePath));
            String line = reader.readLine();
            while (line != null) {
                String[] oneLine = line.split("\\s+");
                double[] x = Arrays.stream(oneLine[0].split(",")).mapToDouble(Double::parseDouble).toArray();
                double[] y = Arrays.stream(oneLine[1].split(",")).mapToDouble(Double::parseDouble).toArray();
                double[][] xx = new double[x.length][1];
                double[][] yy = new double[y.length][1];

                for (int i = 0; i < x.length; i++) {
                    xx[i][0] = x[i];
                }
                for (int i = 0; i < y.length; i++) {
                    yy[i][0] = y[i];
                }
                My2DMatrix X = new My2DMatrix(xx);
                My2DMatrix Y = new My2DMatrix(yy);
                Example example = new Example(X, Y);
                result.add(example);


                // read next line
                line = reader.readLine();
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }



    public Example getAtIndex(int index){

        assert index >= 0 && index < examples.size();
        return examples.get(index);
    }

    public int size(){
        return examples.size();
    }
    
    public List<My2DMatrix> getX(){
        List<My2DMatrix> x = new ArrayList<>();

        for (int i = 0; i < examples.size() ; i++) {
            x.add(examples.get(i).getX());
        }
        return x;
    }
    public List<My2DMatrix> getY(){
        List<My2DMatrix> y = new ArrayList<>();

        for (int i = 0; i < examples.size() ; i++) {
            y.add(examples.get(i).getY());
        }
        return y;
    }
}
