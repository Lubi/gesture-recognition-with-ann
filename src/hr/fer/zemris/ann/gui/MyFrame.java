package hr.fer.zemris.ann.gui;

import hr.fer.zemris.ann.eventhandler.IGUIEventHandler;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class MyFrame extends JFrame {
    JButton clearBtn, confirmBtn, endBtn;
    JLabel howMuchMore;
    JComboBox<String> comboBox;
    DrawingArea drawingArea;
    private final String[] gestures;
    IGUIEventHandler eventHandler;

    ActionListener actionListener = new ActionListener() {
        public void actionPerformed(ActionEvent e) {
            JComponent source = (JComponent) e.getSource();
            if (e.getSource() == clearBtn) {
                drawingArea.clear();
            } else if (e.getSource() == confirmBtn) {
                String message = eventHandler.confirm(drawingArea.getGesturePath(), comboBox.getSelectedItem().toString());

                howMuchMore.setText(message);
                drawingArea.clear();
            } else if (e.getSource() == endBtn) {


                String message  = eventHandler.end();
                howMuchMore.setText(message);
            }
        }
    };


    private void makeWarning() {
        String warningMessage = "Now change gesture type or end!!";
        JOptionPane.showMessageDialog(null, warningMessage, "warningMessage: ", JOptionPane.WARNING_MESSAGE);
    }

    public MyFrame(String[] gesturesIn, IGUIEventHandler guiEventHandler) {
        super("Gesture input");
        gestures = gesturesIn;
        this.eventHandler = guiEventHandler;
        configure();
    }

    private void configure() {
        Container content = getContentPane();
        content.setLayout(new BorderLayout());
        drawingArea = new DrawingArea();

        content.add(drawingArea, BorderLayout.CENTER);


        JPanel controls = new JPanel();

        comboBox = new JComboBox<>(gestures);

        comboBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (e.getSource() == comboBox) {
                    String message =  eventHandler.changedComboBoxValue(comboBox.getSelectedItem().toString());
                    howMuchMore.setText(message);
                }
            }
        });
        controls.add(comboBox);

        endBtn = new JButton("End");
        endBtn.addActionListener(actionListener);
        confirmBtn = new JButton("Confirm");
        confirmBtn.addActionListener(actionListener);
        clearBtn = new JButton("Clear");
        clearBtn.addActionListener(actionListener);


        controls.add(confirmBtn);
        controls.add(clearBtn);
        controls.add(endBtn);

        content.add(controls, BorderLayout.NORTH);


        howMuchMore = new JLabel("Enter gesture!", SwingConstants.CENTER);
        howMuchMore.setPreferredSize(new Dimension(getWidth(), 70));
        content.add(howMuchMore, BorderLayout.SOUTH);


        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setSize(700, 700);


    }


}
