package hr.fer.zemris.ann.gui;

import hr.fer.zemris.ann.structures.MyPoint;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.util.ArrayList;
import java.util.List;

public class DrawingArea extends JComponent {

    private Image canvas;

    private Graphics2D g2;

    private int currentX, currentY, oldX, oldY;

    private List<MyPoint> gesturePath;

    public DrawingArea() {
        setDoubleBuffered(false);
        addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent e) {
                gesturePath = new ArrayList<>();
                MyPoint myPoint = new MyPoint(e.getX(), e.getY());
                gesturePath.add(myPoint);
                oldX = e.getX();
                oldY = e.getY();
            }
        });

        addMouseMotionListener(new MouseMotionAdapter() {
            public void mouseDragged(MouseEvent e) {

                MyPoint myPoint = new MyPoint(e.getX(), e.getY());
                gesturePath.add(myPoint);

                currentX = e.getX();
                currentY = e.getY();

                if (g2 != null) {
                    g2.drawLine(oldX, oldY, currentX, currentY);
                    repaint();
                    oldX = currentX;
                    oldY = currentY;
                }
            }
        });
    }

    protected void paintComponent(Graphics gesture) {
        if (canvas == null) {
            canvas = createImage(getSize().width, getSize().height);
            g2 = (Graphics2D) canvas.getGraphics();
            clear();
        }

        gesture.drawImage(canvas, 0, 0, null);
    }


    public void clear() {
        g2.setPaint(Color.white);
        g2.fillRect(0, 0, getSize().width, getSize().height);
        g2.setPaint(Color.black);
        repaint();
    }


    public List<MyPoint> getGesturePath() {
        return gesturePath;
    }
}
