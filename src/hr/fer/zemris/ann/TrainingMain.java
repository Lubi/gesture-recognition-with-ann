package hr.fer.zemris.ann;

import hr.fer.zemris.ann.activationfunctions.IActivationFunction;
import hr.fer.zemris.ann.activationfunctions.SigmoidActivation;
import hr.fer.zemris.ann.loss.ILossFunction;
import hr.fer.zemris.ann.loss.SquareLoss;
import hr.fer.zemris.ann.structures.Example;
import hr.fer.zemris.ann.structures.My2DMatrix;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class TrainingMain {
    public static void main(String[] args) {
        final int BATCHSIZE = 1; /// best stohastic= eta = 0.05, arh= 2 * M, 20, 5
        final double LEARNINGRATE = 0.05;
        final double EPSILON = 1e-5;
        final int EPOCHS = 50_000;

        // args
        final int discretePointsNum = Integer.parseInt(args[0]);
        List<Integer> hiddenLayersDimension = Arrays.stream(args[1].split(",")).map(Integer::parseInt).collect(Collectors.toList());
        final int greekLettersCount = Integer.parseInt(args[2]);
        final String filePath = args[3];
        final String annStoreFile = args[4];


        ILossFunction lossFunction = new SquareLoss();

        List<Integer> annDimension = new ArrayList<>(List.of(2 * discretePointsNum));
        annDimension.addAll(hiddenLayersDimension);
        annDimension.add(greekLettersCount);

        IActivationFunction activationFunction = new SigmoidActivation();

        Dataset dataset = new Dataset(filePath);

        ANN ann = new ANN(annDimension, lossFunction, activationFunction);
        ann.fit(dataset, BATCHSIZE, EPOCHS, LEARNINGRATE, EPSILON);

        for (int i = 0; i < dataset.size(); i++) {
            Example example = dataset.getAtIndex(i);
            My2DMatrix predicted = ann.predict(example.getX());

            System.out.println("realY= " + example.getY());
            System.out.println("predY= " + predicted);
            System.out.println("--------------------------");

        }

        ann.storeANNToFile(annStoreFile);


    }




}
