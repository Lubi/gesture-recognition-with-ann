package hr.fer.zemris.ann.transform;

import hr.fer.zemris.ann.structures.MyPoint;

import java.util.ArrayList;
import java.util.List;

public class Transformer {

    private int M;


    public Transformer(int m) {
        M = m;
    }

    public List<MyPoint> transformOneGesture(List<MyPoint> toTransformGesture) {
        List<MyPoint> tempTransformed;


        tempTransformed = avgTransform(toTransformGesture);
        double scaler = findMaxCordinate(tempTransformed);
        tempTransformed = nomaliseTransform(tempTransformed, scaler);
        double lengthOfGesture = findLengthOfGesture(tempTransformed);

        return standardiseToMPoints(tempTransformed, lengthOfGesture);
    }

    private List<MyPoint> avgTransform(List<MyPoint> gesturePoints) {
        List<MyPoint> newPoints = new ArrayList<>();
        double sumX = 0;
        double sumY = 0;
        int length = gesturePoints.size();
        for (MyPoint point : gesturePoints) {
            sumX += point.getX();
            sumY += point.getY();
        }
        double meanX = sumX / length;
        double meanY = sumY / length;

        for (MyPoint point : gesturePoints) {
            MyPoint newPoint = new MyPoint((point.getX() - meanX), (point.getY() - meanY));
            newPoints.add(newPoint);
        }

        return newPoints;
    }

    private double findMaxCordinate(List<MyPoint> gesturePoints) {
        double maxX = Math.abs(gesturePoints.get(0).getX());
        double maxY = Math.abs(gesturePoints.get(0).getY());

        for (MyPoint point : gesturePoints) {
            double absX = Math.abs(point.getX());
            double absY = Math.abs(point.getY());
            if (absX > maxX) {
                maxX = absX;
            }
            if (absY > maxY) {
                maxY = absY;
            }
        }
        return Math.max(maxX, maxY);
    }

    private List<MyPoint> nomaliseTransform(List<MyPoint> gesturePoints, double scaler) {
        List<MyPoint> newPoints = new ArrayList<>();
        if (scaler < 1e-6)
            throw new IllegalArgumentException("Scaler m is to small! ");

        for (MyPoint point : gesturePoints) {
            MyPoint newPoint = new MyPoint(point.getX() / scaler, point.getY() / scaler);
            newPoints.add(newPoint);
        }
        return newPoints;
    }

    private double findLengthOfGesture(List<MyPoint> gesturePoints) {
        double length = 0;
        for (int i = 1; i < gesturePoints.size(); i++) {
            MyPoint currentPoint = gesturePoints.get(i);
            MyPoint previousPoint = gesturePoints.get(i - 1);
            double distance = Math.sqrt(Math.pow(previousPoint.getX() - currentPoint.getX(), 2) + Math.pow(previousPoint.getY() - currentPoint.getY(), 2));
            length += distance;
        }
        return length;
    }
    // ide do M-2, ne do M-1: popravi uvjet u for petlji metode interpolateWithNearestPoints
    private List<MyPoint> standardiseToMPoints(List<MyPoint> gesturePoints, double lengthOfGestureD) {
        List<MyPoint> newPoints = new ArrayList<>();
        for (int k = 0; k < M-1; k++) {
            double distanceFromFirst = (lengthOfGestureD * k) / (M - 1);
            MyPoint nearest = interpolateWithNearestPoints(gesturePoints, distanceFromFirst);
            newPoints.add(nearest);
        }
        newPoints.add(gesturePoints.get(M-1));
        return newPoints;
    }

    private MyPoint interpolateWithNearestPoints(List<MyPoint> gesturePoints, double distanceFromFirst) {
        double distance = 0;
        int i;
        for (i = 1; distance < distanceFromFirst; i++) {
            MyPoint currentPoint = gesturePoints.get(i);
            MyPoint previousPoint = gesturePoints.get(i - 1);
            double distanceBeetween2P = Math.sqrt(Math.pow(previousPoint.getX() - currentPoint.getX(), 2) + Math.pow(previousPoint.getY() - currentPoint.getY(), 2));
            distance += distanceBeetween2P;
        }
        double nextX = gesturePoints.get(i).getX();
        double nextY = gesturePoints.get(i).getY();
        double previousX = gesturePoints.get(i - 1).getX();
        double previousY = gesturePoints.get(i - 1).getY();

        return new MyPoint((nextX + previousX) / 2, (nextY + previousY) / 2);
    }


}
