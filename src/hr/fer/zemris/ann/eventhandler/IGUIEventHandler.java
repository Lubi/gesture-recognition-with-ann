package hr.fer.zemris.ann.eventhandler;

import hr.fer.zemris.ann.structures.MyPoint;

import java.util.List;

public interface IGUIEventHandler {
    /**
     *
     * @param gesture List of points of gesture
     * @return message to show on GUI
     */
    String confirm(List<MyPoint> gesture, String gestureName);


    /**
     *
     * @return message for GUI
     */
    String end();

    String changedComboBoxValue(String newType);
}
