package hr.fer.zemris.ann.eventhandler;

import hr.fer.zemris.ann.structures.MyPoint;
import hr.fer.zemris.ann.transform.Transformer;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class InputGUIEventHandler implements IGUIEventHandler {
    private final int pointsNum;
    private Transformer transformer;
    private HashMap<String, List<List<MyPoint>>> typeGesturePoints = new HashMap<>();
    private String filePath;
    private HashMap<String, String> gestureCodes;
    private int counter;


    public InputGUIEventHandler(Transformer transformer, String filePath, HashMap<String, String> gestureCodes, int pointsNum) {
        this.transformer = transformer;
        this.filePath = filePath;
        this.gestureCodes = gestureCodes;
        this.pointsNum = pointsNum;
        this.counter = pointsNum;
    }

    @Override
    public String confirm(List<MyPoint> gesture, String gestureName) {
        if (!typeGesturePoints.containsKey(gestureName)) {
            if (gestureCodes.containsKey(gestureName)) {
                List<List<MyPoint>> newList = new ArrayList<>();
                newList.add(gesture);
                typeGesturePoints.put(gestureName, newList);
            } else throw new InvalidParameterException("Taj tip geste ne postoji!!");
        } else typeGesturePoints.get(gestureName).add(gesture);


        String message;
        counter -= 1;
        if (counter == 0) {
            message = "Change gesture name!!!";
        } else message = counter + " much more gestures of this type!";

        return message;
    }

    @Override
    public String end() {
        HashMap<String, List<List<MyPoint>>> transformedGesture = transformGestures();
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(filePath));

            for (String gestureName : transformedGesture.keySet()) {

                for (List<MyPoint> gesturePoints : transformedGesture.get(gestureName)) {
                    String pointsString = gesturePoints.stream()
                            .map(MyPoint::toString)
                            .collect(Collectors.joining(","));
                    String codeForGesture = gestureCodes.get(gestureName);

                    writer.write(pointsString + "\t" + codeForGesture + "\n");
                }

            }

            writer.close();

        } catch (IOException e) {
            e.printStackTrace();
        }


        return "Successful saved to file! Click exit to finish!";
    }

    private HashMap<String, List<List<MyPoint>>> transformGestures() {
        HashMap<String, List<List<MyPoint>>> transformed = new HashMap<>();
        for (String gesture : typeGesturePoints.keySet()) {
            List<List<MyPoint>> transformedOneGesture = transformOneGesture(typeGesturePoints.get(gesture));
            transformed.put(gesture, transformedOneGesture);
        }
        return transformed;
    }

    private List<List<MyPoint>> transformOneGesture(List<List<MyPoint>> lists) {
        List<List<MyPoint>> result = new ArrayList<>();

        for (List<MyPoint> oneGesture : lists) {
            result.add(transformer.transformOneGesture(oneGesture));
        }
        return result;
    }

    @Override
    public String changedComboBoxValue(String newType) {
        counter = pointsNum;
        return counter + " much more gestures of this type!";
    }
}
