package hr.fer.zemris.ann.eventhandler;

import hr.fer.zemris.ann.ANN;
import hr.fer.zemris.ann.structures.My2DMatrix;
import hr.fer.zemris.ann.structures.MyPoint;
import hr.fer.zemris.ann.transform.Transformer;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class TesterGUIEventHandler implements IGUIEventHandler {
    Transformer transformer;
    HashMap<String, String> gestureCodes;
    ANN ann;

    public TesterGUIEventHandler(Transformer transformer, HashMap<String, String> gestureCodes, ANN ann) {
        this.ann = ann;
        this.gestureCodes = gestureCodes;
        this.transformer = transformer;
    }

    @Override
    public String confirm(List<MyPoint> gesture, String gestureName) {
        List<MyPoint> transfGest = transformer.transformOneGesture(gesture);


        double[][] x = new double[transfGest.size() * 2][1];
        for (int i = 0; i < transfGest.size(); i++) {
            x[2 * i][0] = transfGest.get(i).getX();
            x[2 * i + 1][0] = transfGest.get(i).getY();
        }


        My2DMatrix rez = ann.predict(new My2DMatrix(x));
        System.out.println(rez);

        return convertRezToCode(rez);
    }

    private String convertRezToCode(My2DMatrix rez) {
        int[] predictedCode = new int[rez.numberOfRows()];

        List<Integer> maxIndices = rez.getIndexOfMaxVal();
        assert maxIndices.get(1) == 0;
        predictedCode[maxIndices.get(0)] = 1;
        String codeResult = Arrays.stream(predictedCode)
                .mapToObj(String::valueOf)
                .collect(Collectors.joining(","));
        return gestureCodes.get(codeResult);
    }

    @Override
    public String end() {
        return null;
    }

    @Override
    public String changedComboBoxValue(String newType) {
        return null;
    }
}
