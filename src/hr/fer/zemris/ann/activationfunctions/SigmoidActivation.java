package hr.fer.zemris.ann.activationfunctions;

import hr.fer.zemris.ann.structures.My2DMatrix;

import java.io.Serializable;

public class SigmoidActivation implements IActivationFunction, Serializable {
    private static final long serialVersionUID = 2L;
    @Override
    public My2DMatrix activate(My2DMatrix vector) {
        assert vector.numberOfCols() == 1;

        double[][] newVals = new double[vector.numberOfRows()][1];
        for (int i = 0; i < vector.numberOfRows() ; i++) {
            newVals[i][0] = 1.0 / (1+ Math.exp((-1) * vector.getValueAt(i, 0)));
        }
        return new My2DMatrix(newVals);
    }
}
