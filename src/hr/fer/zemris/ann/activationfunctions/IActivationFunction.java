package hr.fer.zemris.ann.activationfunctions;

import hr.fer.zemris.ann.structures.My2DMatrix;

public interface IActivationFunction {
    My2DMatrix activate(My2DMatrix vector);
}
