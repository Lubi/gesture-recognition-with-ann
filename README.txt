Program description:

Program consists of 3 parths: InputMain.java, TrainingMain.java and TesterMain.java.

In InputMain we build dataset of gesture instances. Needed to add 3 program arguments:
number of gestures needed to enter per greek letter,
number of points per gesture that are taken into acccount (because need of discretization of continuos gesture) and
file path where gesture points are stored.
Making gestures procedure:
1. From drop down menu select greek letter you want to enter.
2. Enter gesture
3. Click confirm button to store entered gesture if you are satisfied with gesture or clear button to reenter gesture.
4. Change greek letter when enter all for one class
5. Click end button when entered all gestures for all letters

In TrainingMain ANN is learned for gesture recognition. Needed to add 5 program arguments:
number of points that are taken into acccount (because need of discretization of continuos gesture),
number of neurons per hidden layers (e.g. 20,5 -> 2 hidden layers with 20 and 5 neurons),
number of output classes (number of greek letters),
file path where gestures are stored and
file path where ann parameters will be stored.

Number of input neurons is always 2 * number of points per gesture, because x and y coordinates are taken for each point.


In TesterMain user enter gesture and click confirm to employ ANN for classification in one of greek letters.
